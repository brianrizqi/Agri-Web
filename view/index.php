<!DOCTYPE html>
<html lang="en">
<head>
    <title>Agri</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="../resources/css/style.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-between">

    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="v_product.php">Product</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="v_contact.php">Contact</a>
        </li>
    </ul>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="v_register.php">Sign Up</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="v_login.php">Sign In</a>
        </li>
    </ul>
</nav>
<div id="banner">

    <div class="center">

        <h1>Welcome to Agristock</h1>
        <h2>Do you want to buy?</h2>
        <a href="#" class="btn-link">Learn More</a>

    </div>

</div>

<div class="container">
    <div class="row">
        <?php
        for ($i = 0; $i < 4; $i++) {
            ?>
            <div class="col-sm-3">
                <div class="card_home">
                    <div class="card" style="width:250px">
                        <img class="card-img-top" src="../resources/images/Logo.png" alt="Card image">
                        <div class="card-body">
                            <h4 class="card-title">Cabe</h4>
                            <p class="card-text">Cabe adalah blablablabla</p>
                            <a href="#" class="btn btn-success">Buy</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
</body>
</html>