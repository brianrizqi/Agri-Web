<html>
<head>
    <title>Sign Up</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../resources/css/style.css" type="text/css">
</head>
<body class="body_regis">
<div class="regis-box">
    <div class="regis-left-box">
        <h1>Sign Up</h1>
        <form>
            <input type="text" name="nama" placeholder="Nama">
            <input type="email" name="email" placeholder="Email">
            <input type="text" name="username" placeholder="Username">
            <input type="password" name="password" placeholder="Password">
            <input type="submit" name="regis" value="Register">
        </form>
    </div>
    <div class="regis-right-box">
        <span class="signinwith"> Sign in with <br> Social Network
        </span>

        <button class="social facebook" name="facebook">Log In with Facebook</button>
        <button class="social google-plus" name="facebook">Log In with Google+</button>
        <button class="social twitter" name="facebook">Log In with Twitter</button>
    </div>
    <div class="or">OR</div>
</div>
</body>
</html>